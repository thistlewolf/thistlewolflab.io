<?php if(!defined('KIRBY')) exit ?>

username: thistlewolf
password: >
  $2a$10$PVxG.QF5FUVEAEZiOwOhH.QO9tSVeVmCcr6HsgrfSnSWkHo9TkvqC
email: thisdoug@gmail.com
language: en
role: admin
